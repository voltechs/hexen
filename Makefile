PLAT = MacOSX
LDFLAGS = -lcurses
OBJS = \
	main.o\
	Packet.o\
	Server.o\
	Client.o\
	Hexen.o

all: Hexen

main.o: main.cpp main.h
	g++ -g -c main.cpp -o main.o

Packet.o: src/Packet.cpp src/Packet.h
	g++ -g -c src/Packet.cpp -o Packet.o

Server.o: src/Server.cpp src/Server.h
	g++ -g -c src/Server.cpp -o Server.o

Client.o: src/Client.cpp src/Client.h
	g++ -g -c src/Client.cpp -o Client.o

Hexen.o: src/Hexen_$(PLAT).cpp src/Hexen_$(PLAT).h
	g++ -g -c src/Hexen_$(PLAT).cpp -o Hexen.o
	
Hexen: $(OBJS)
	g++ -g $(LDFLAGS) -o Hexen $(OBJS)

clean:
	rm -f $(OBJS)
	@echo "Build files cleaned"
