#include "main.h"

Hexen *client;

int main (int argc, char * const argv[])
{
#ifdef __WIN32__
	WSADATA wsaData;
	WORD version;
	int error;

	version = MAKEWORD( 2, 0 );

	error = WSAStartup( version, &wsaData );

	/* check for error */
	if ( error != 0 )
	{
		/* error occured */
		return FALSE;
	}

	/* WinSock has been initialized */
#else
	
	signal(SIGWINCH, resize);
	//signal(SIGKILL, terminate);
	
#endif

	if (argc > 1 && !strcmp(argv[1], "server")) {

		Server server = Server(1, argc, argv, false);
		server.run();
		printf("Server");
		system("PAUSE");

	} else {
		
		if (argc == 2)
			client = new Hexen(argv[1]);
		else client = new Hexen();
		client->run();
	}
	return 0;
}
/*
void terminate(int sig) {
	// Yes, first ctl-c will end any client,
	// second ctl-c will end the server.
	if (client) client.disconnect;
	else if (server) server.disconnect;
 }
*/

void resize(int sig) {
	client->resize();
}
