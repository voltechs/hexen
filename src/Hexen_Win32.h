#include "Client.h"

#define KEY_DELETE 127

//typedef void ( *sig_t ) ( void );

class Hexen : Client {

public:
	Hexen( void );
	Hexen( std::string );
	Hexen( std::string, std::string );
	~Hexen( void );
	void run( void );
	void quit( void );
private:

	WINDOW *window;
	int rows, cols;

	void init( void );
	void startupScreen( bool );
	void catchInput( void );
	void updateToolbar( void );
	void drawConversation( void );
	void addLine( int, std::string );
	void addLine( int, int, std::string );
	void clearTextField( void );
	void updateWindow( void );
	void resize( void );

	// Keyboard
	void keyTyped( int ch );
	void keyDelete( void );
	void keyLeft( void );
	void keyRight( void );
	void keyUp( void );
	void keyDown( void );
	void keyEnter( void );
};
