#include "Client.h"

#ifdef __WIN32__
	// For getting username
	#include <windows.h>
	#include <Lmcons.h>
#endif
//typedef void ( *sig_t ) ( void );

#define PRODUCT_NAME "NUChat"
#define PRODUCT_VERSION "1.3"

class Hexen : Client {

public:
	Hexen( void );
	Hexen( std::string );
	Hexen( std::string, std::string );
	~Hexen( void );
	void run( void );
	void quit( void );
	void resize( void );
	
private:

	WINDOW *window;
	int rows, cols;
	// For the toolbar and input line
	unsigned int chrome;

	void init( void );
	void startupScreen( bool );
	void catchInput( void );
	void updateToolbar( void );
	void drawConversation( void );
	void addLine( int, std::string );
	void addLine( int, int, std::string );
	void clearTextField( void );
	void updateWindow( void );

	// Keyboard
	void keyTyped( int ch );
	void keyDelete( void );
	void keyLeft( void );
	void keyRight( void );
	void keyUp( void );
	void keyDown( void );
	void keyEnter( void );
};
