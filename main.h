#pragma once

#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <unistd.h>

// Server Class
#include "Server.h"

// OS Dependent Client
#include "Hexen.h"

void terminate(int sig);
void resize(int sig);
